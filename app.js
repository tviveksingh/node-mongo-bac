const express = require('express');
const jwt = require('jsonwebtoken');
const bodyparser = require('body-parser');
const mongoose = require('./src/db');
const {Users, BracketsCount} = require('./src/models');
const { verifytoken, checkBalanced, jwtPrivateKey, isValidDate } = require('./src/utils');

const app = express();
app.use(bodyparser.urlencoded({extended: true}))

/* 
    Register a user
*/
app.post('/register', (req, res)=>{
    let {name, email, password, dob, role} = req.body
    let date = new Date(dob);;
    if(!isValidDate(date)){
        res.send({
            error: 'Enter Date in MM/DD/YYYY or YYYY/MM/DD format'
        })
    }
    Users.findOne({email}, (err, doc)=>{
        if(!err){
            if(!doc){
                let user = new Users({ name, email, dob: date, role, password });
                user.save((err, doc)=>{
                    if(!err){
                        res.send({
                            message: 'success'
                        })
                    } else {
                        res.send({
                            error: err
                        })
                    }
                })
            } else {
                res.send({
                    message: `User with email ${email} already exists.`
                });
            }
        } else {
            res.send({
                error: err
            });
        }
    });    
});

/* 
    Login using email and password
*/

app.post('/login', (req, res)=>{
    let { email, password } = req.body
    console.log(email)
    Users.findOne({email: `${email}`}, (findErr, result)=>{
        if(!findErr){
            if(result){
                if(result.password === password){
                    jwt.sign(
                        {
                            email: result.email,
                            role: result.role,
                            dob: result.dob
                        }, 
                        jwtPrivateKey,
                        (jwtErr, token) => {
                            if(!jwtErr){
                                res.send({
                                    message: 'success',
                                    token
                                });
                            } else {
                                res.send({
                                    error: jwtErr
                                });
                           }
                        }
                    );
                } else {
                    res.send({
                        email,
                        message: 'user/password incorrect'
                    })
                }
            } else {
                res.send({
                    email,
                    error: 'User Not Found. Kindly Register.'
                })
            }
        } else {
            res.send({
                error: findErrs
            })
        }
    });
});

/**
 * Checking if brackets are balanced
 */
app.post('/balanced', verifytoken, (req, res)=>{
    const { brackets } = req.body;
    const { email } = req.authData;
    let username;
    let message = checkBalanced(brackets);
    username = email;
    // Finding the user and updating the attemps
    // if user is not present it is created
    // findOneAndUpdate(query, update, options, callback)
    BracketsCount.findOneAndUpdate(
        {email},
        { $set: {username: email}, $inc: {attemps:1} },
        {upsert: true, new: true, useFindAndModify: false},
        (error, doc)=>{
            if(!error) {
                let { attemps } = doc
                res.send({
                    username,
                    message,
                    attemps
                }); 
            } else {
                res.send({
                    username,
                    error: error
                });
            }
        }
    );
});

/**
 * Delete the user specified (admin only)
 */
app.post('/delete', verifytoken, (req, res)=>{
    const { role } = req.authData
    if(role === 'admin'){
        const { userEmail } = req.body
        Users.findOne({email: userEmail}, (error, doc)=>{
            if(!error){         
                if(doc){
                    Users.deleteOne({email: doc.email}, (error, message)=>{
                        if(!error){
                            res.send({
                                message: 'Successfully deleted.'
                            });
                        } else {
                            res.send({
                                message: 'database error',
                                error: error
                            });
                        }
                    })
                } else {
                    res.send({
                        message: 'user not found',
                        error: error
                    });
                }
            } else {
                res.send({
                    message: 'query error',
                    error: error
                });
            }
        })
    } else {
        res.send({
            message: 'Login as Admin'
        })
    }
});
/**
 * List all the users specified (admin only)
 */
app.post('/users', verifytoken, (req, res)=>{
    const { role } = req.authData
    if(role === 'admin') {
        Users.find((err, docs)=>{
            if(!err){
                res.send({
                    users: docs
                })
            } else {
                res.send({
                    message: err
                })
            }
        });
    }
});

app.listen(5000, () => console.log('Server Started on http://localhost:5000/'))