const mongoose = require('mongoose');
let Schema = mongoose.Schema;

var Users =  mongoose.model('users', new Schema({
    name: String,
    dob: Date,
    email: String,
    password: String,
    role: String
}));

var BracketsCount = mongoose.model('bracketsCount', new Schema({
    email: String,
    attemps: Number 
}));

module.exports = {
    Users,
    BracketsCount
}