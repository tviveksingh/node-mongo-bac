let mongoose = require('mongoose');
const url = 'mongodb://localhost/test'
mongoose.connect(url, { useNewUrlParser: true });

module.exports = {
    mongoose
}