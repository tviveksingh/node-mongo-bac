# NodeJS / Mongo DB Task (BigAppCompany)
----
## Setup
* Install NodeJS [Download](https://nodejs.org/en/download/)

* Installing MongoDB [Instalation Guide](https://docs.mongodb.com/manual/administration/install-community/)
* Clone the repo

        git clone https://tviveksingh@bitbucket.org/tviveksingh/node-mongo-bac.git

* Install dependencies

        npm install

* Start the server

        npm start

> Server will be started at port 5000.

----
## API endpoints
> All are POST.

* **/login** 
  * email
  * password
* **/register**
  * name
  * email
  * password
  * dob
  * role
* **/users**:
  * *headers*
    * authorization: {admin token}
* **/delete**
  * *headers*
    * authorization: {admin token}
  * *body*
    * email
* **/balanced**
   * header
    * authorization
   * body
    * brackets

____
## Suggestions
* Verify Parameters such as email, password, date
* Only admin can create new admins